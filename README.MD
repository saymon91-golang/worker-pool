# Worker pool

### Description
Library for managing parallel functions execution.

### Installation
```shell
go get -u gitlab.com/saymon91-golang/worker-pool
```

### Usage

```go
package main

import (
	"errors"
	"fmt"
	"time"
	"context"
	"sync"
	"sync/atomic"

	pool "gitlab.com/saymon91-golang/worker-pool/pkg"
)

func main() {
	p := pool.NewPool(&pool.Options{Size: 10})
	defer p.Close()

	wg := new(sync.WaitGroup)
	wg.Add(1000)
	
	var count int32
	
	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()

	go func () {
		for {
			_, ok := <- ticker.C
			if !ok {
				return
            }

			stats := p.Stat()
			fmt.Println("pool statistic", stats.Size, stats.Idle)
        }
    }()
	
	for i := 0; i < 1000; i++ {
		go func(i int) {
			defer wg.Done()
			if i%2 == 0 {
				if err := p.Do(func() {
					<-time.After(1 * time.Second)
					atomic.AddInt32(&count, 1)
				}); err != nil {
					fmt.Println("error do task", i, err)
                }
			} else {
				ctx, _ := context.WithTimeoutCause(context.Background(), 1*time.Second, errors.New("wait time is left"))
				if err := p.DoContext(ctx, func() {
					<-time.After(1 * time.Second)
					atomic.AddInt32(&count, 1)
				}); err != nil {
					fmt.Println("error do context managed task", i, err)
                }
			}
		}(i)
	}
	
	wg.Wait()

	fmt.Println("all task is done", count)
}
```