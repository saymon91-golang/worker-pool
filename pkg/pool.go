package worker_pool

import (
	"context"
	"errors"
	"fmt"
	"runtime"
	"sync/atomic"
)

var (
	ErrPanic          = errors.New("runtime panic")
	ErrPoolIsFinished = errors.New("pool is finished")
	ErrContextIsDone  = errors.New("context is done")
)

type Func func()

type Stat struct {
	Size    int32
	Waiting int32
	Idle    int32
	Stopped bool
}

type Pool interface {
	// Do - method gets function to call when have got a worker
	// Method returns error ErrPoolIsFinished if pool instance is stopped
	// Method returns joined error ErrPanic when panic is caught
	Do(Func) error

	// DoContext - is context managed version of method Do
	// Method returns error ErrPoolIsFinished if pool instance is stopped
	// Method returns joined error ErrPanic when panic is caught
	// Method returns ErrContextIsDone when passed context is done and has no cause
	// Method returns cause from passed context when it is done
	DoContext(context.Context, Func) error

	// Close - method closes the pool instance and finish processing
	Close()

	// Stat - returns Stat instance
	Stat() *Stat
}

type pool struct {
	size    int32
	waiting int32
	idle    int32
	workers chan int32
	closed  int32
}

func (p *pool) release(w int32) {
	if p.closed == 0 {
		p.workers <- w
	}
	atomic.AddInt32(&p.idle, 1)
	if p.idle == p.size {
		close(p.workers)
	}
}

func exec(fn Func) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = errors.Join(ErrPanic, fmt.Errorf("panic: %v", e))
		}
	}()

	fn()
	return
}

func (p *pool) Do(fn Func) error {
	if p.closed > 0 {
		return ErrPoolIsFinished
	}

	atomic.AddInt32(&p.waiting, 1)

	w, ok := <-p.workers
	atomic.AddInt32(&p.waiting, -1)
	if !ok {
		return ErrPoolIsFinished
	}

	atomic.AddInt32(&p.idle, -1)

	defer p.release(w)
	return exec(fn)
}

func (p *pool) DoContext(ctx context.Context, fn Func) error {
	if ctx == nil {
		return p.Do(fn)
	}

	if p.closed > 0 {
		return ErrPoolIsFinished
	}

	atomic.AddInt32(&p.waiting, 1)

	select {
	case <-ctx.Done():
		atomic.AddInt32(&p.waiting, -1)
		if err := ctx.Err(); err != nil {
			return err
		} else {
			return ErrContextIsDone
		}
	case w, ok := <-p.workers:
		atomic.AddInt32(&p.waiting, -1)
		if !ok {
			return ErrPoolIsFinished
		}

		atomic.AddInt32(&p.idle, -1)

		defer p.release(w)

		if err := ctx.Err(); err != nil {
			return err
		}

		return exec(fn)
	}
}

func (p *pool) Stat() *Stat {
	return &Stat{
		Size:    p.size,
		Waiting: p.waiting,
		Idle:    p.idle,
		Stopped: p.closed > 0,
	}
}

func (p *pool) Close() {
	if atomic.CompareAndSwapInt32(&p.closed, 0, 1) && p.idle == p.size {
		close(p.workers)
	}
}

type Options struct {
	Size uint
}

// NewPool = method returns the Pool instance
func NewPool(options *Options) Pool {
	var size int32
	if options == nil || options.Size < 1 {
		size = int32(runtime.NumCPU())
	} else {
		size = int32(options.Size)
	}

	workers := make(chan int32, size)
	mx := int(size)
	for w := 0; w < mx; w++ {
		workers <- int32(w)
	}

	return &pool{
		size:    size,
		idle:    size,
		workers: workers,
	}
}
