package worker_pool

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestPool(t *testing.T) {
	p := NewPool(&Options{Size: 10})

	wg := new(sync.WaitGroup)
	wg.Add(1000)
	for i := 0; i < 1000; i++ {
		go func(i int) {
			defer wg.Done()

			ctx, _ := context.WithTimeoutCause(context.Background(), 10*time.Second, fmt.Errorf("time is left"))
			if err := p.DoContext(ctx, func() {
				t.Log("start task on worker", i)
				<-time.After(1 * time.Second)
			}); err != nil {
				t.Log("error get worker timeout", i, err)
			}
		}(i)
	}

	<-time.After(5 * time.Second)
	p.Close()

	if err := p.Do(func() {
		<-time.After(100 * time.Second)
		t.Fatal("task must not be called")
	}); err != nil {
		t.Log("correct error when do closed pool", err)
	}

	wg.Wait()
	t.Log("success!")
}
